package cql.ecci.ucr.ac.cr.ejemplorefactoring;

public class Buses {

    public static final String[] TITLES = {
            "UCR001",
            "UCR002",
            "UCR003"
    };

    public static final String[] DETAILS = {
            "80 pasajeros, " +
                    "Aire acondicionado, " +
                    "Motor de baja emisión de gases, " +
                    "Sistema de Audio.",

            "70 pasajeros, " +
                    "Aire acondicionado, " +
                    "Motor de baja emisión de gases, " +
                    "Sistema de Audio.",

            "60 pasajeros, " +
                    "Aire acondicionado, " +
                    "Motor de baja emisión de gases, " +
                    "Sistema de Audio."
    };
}
