package cql.ecci.ucr.ac.cr.ejemplorefactoring;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button buttonFragmentos = findViewById(R.id.buttonFragmentos);

        buttonFragmentos.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                irFragmentos();
            }
        });
    }

    private void irFragmentos(){
        Intent intent = new Intent(this, EjemploFragmentosActivity.class);
        startActivity(intent);
    }
}
